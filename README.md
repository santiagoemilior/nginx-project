# Project Overview

This project consists of three main components:

   * An Autoscaling group (Nginx app servers)
   * An Application Load Balancer (ALB)
   * An RDS MySQL Database

## Description

Using the provided code you will deploy an autoscaling group on 3 different AZs for high availability, all behind an internet facing application load balancer. You will also deploy a RDS database running MySql. The database only allows ingress traffic from the webserver running Nginx created by the autoscaling group.


## Usage Instructions

- [ ] Add all the required values of the .tfvars file.
- [ ] Make sure your database master password is 8 characters long.

