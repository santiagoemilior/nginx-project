// -----------------------------------------------
// VPC/General values
// -----------------------------------------------

region         = "us-east-1"   // Ex: us-east-1, us-west-1 ...
project_name   = "appserver"   // Ex: Add project name ...
vpc_cidr       = "10.0.0.0/16" // Ex: 10.0.x.x, 172.31.x.x ...
environment    = "dev"         // Ex: dev, stage, prod, test ...
allowed_ip_alb = "0.0.0.0/0"   // Enter your public IP

// -----------------------------------------------
// DB values
// -----------------------------------------------

ec2_web_tag_name = "nginx server" // Additional tag to add to app servers
identifier       = "db-mysql"     // DB instance id
engine           = "mysql"        // DB engine 
engine_version   = "8.0.27"       // DB engive version
db_name          = "dbmysql"      // DB name
db_storage_size  = 20             // DB storage size in GB
instance_class   = "db.t2.micro"  // Ex: db.t2.micro, db.m5d.large ....
password         = "password"     // Enter database master password
username         = "username"     // Enter database master username

// -----------------------------------------------
// ASG values
// -----------------------------------------------

desired-capacity  = 1          // Enter desired capacity of ASG ....
max-capacity      = 2          // Enter maximum capacity of ASG ....
min-capacity      = 1          // Enter minimum capacity of ASG ....
web_instance_type = "t2.micro" // Ex: t2.micro, c4.large ....