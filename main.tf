// --------------------------------------------------
// provider info
// --------------------------------------------------

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region                  = var.region
  shared_credentials_file = "/Users/santiagorivera/.aws/credentials"
  profile                 = "san-student"
}

// --------------------------------------------------
// VPC module
// --------------------------------------------------
module "vpc" {
  source       = "./modules/vpc"
  project_name = var.project_name
  vpc_cidr     = var.vpc_cidr
  environment  = var.environment
}

// --------------------------------------------------
// Security Group module
// --------------------------------------------------
module "security-group" {
  source         = "./modules/sg"
  vpc_id         = module.vpc.vpc_id
  environment    = var.environment
  project_name   = var.project_name
  allowed_ip_alb = var.allowed_ip_alb
}

// --------------------------------------------------
// ASG module
// --------------------------------------------------
module "asg" {
  source             = "./modules/asg"
  ec2_web_tag_name   = var.ec2_web_tag_name
  ec2_security_group = module.security-group.ec2_security_group
  subnet_ids         = module.vpc.subnet_ids
  alb_security_group = module.security-group.alb_security_group
  environment        = var.environment
  vpc_id             = module.vpc.vpc_id
  project_name       = var.project_name
  desired-capacity   = var.desired-capacity
  max-capacity       = var.max-capacity
  min-capacity       = var.min-capacity
  web_instance_type  = var.web_instance_type
  private_subnet_ids = module.vpc.private_subnet_ids

}

// --------------------------------------------------
// Database module
// --------------------------------------------------

module "database" {
  source                  = "./modules/rds"
  identifier              = var.identifier
  engine                  = var.engine
  engine_version          = var.engine_version
  db_name                 = var.db_name
  password                = var.password
  database-security-group = module.security-group.database-security-group
  private_subnet1_az1     = module.vpc.private_subnet1_az1
  private_subnet1_az2     = module.vpc.private_subnet1_az2
  db_storage_size         = var.db_storage_size
  project_name            = var.project_name
  instance_class          = var.instance_class
  username                = var.username
  environment             = var.environment
}