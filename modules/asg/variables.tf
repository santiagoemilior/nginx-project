// --------------------------------------------------
// ASG variables
// --------------------------------------------------

variable "project_name" {
  type        = string
  description = "This configures the project name"
}
variable "vpc_id" {
  type        = string
  description = "This configures the vpc id"
}
variable "alb_security_group" {
  type        = string
  description = "This configures the security group of the load balancer"
}
variable "environment" {
  type        = string
  description = "Environment tag"
}
variable "web_instance_type" {
  type        = string
  description = "This configures app server type"
}
variable "ec2_web_tag_name" {
  type        = string
  description = "This adds a tag to the intances"
}
variable "ec2_security_group" {}
variable "subnet_ids" {
  type = list(string)
}
variable "desired-capacity" {
  type        = number
  description = "This configures the desired capacity of our asg"
}
variable "max-capacity" {
  type        = number
  description = "This configures the max capacity of our asg"
}
variable "min-capacity" {
  type        = number
  description = "This configures the min capacity of our asg"
}
variable "private_subnet_ids" {
  description = "List of private subnets"
  type        = list(string)
}