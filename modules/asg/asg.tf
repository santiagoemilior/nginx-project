// ----------------------------------------------------------------------
// fetch latest amazon 2 linux ami
// ----------------------------------------------------------------------

data "aws_ami" "linux2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

// ----------------------------------------------------------------------
// launch Template for auto scaling group
// ----------------------------------------------------------------------
resource "aws_launch_template" "app_server" {
  name                   = "${var.project_name}-template"
  description            = "App server v1"
  image_id               = data.aws_ami.linux2.id
  instance_type          = var.web_instance_type
  vpc_security_group_ids = [var.ec2_security_group]
  user_data              = filebase64("userdata.sh")
  tag_specifications {
    resource_type = "instance"
    tags = {
      Name        = "${var.project_name}-server"
      Environment = var.environment
    }
  }
}

// ----------------------------------------------------------------------
// auto scaling group
// ----------------------------------------------------------------------
resource "aws_autoscaling_group" "asg" {
  name              = "${var.project_name}-asg"
  desired_capacity  = var.desired-capacity
  max_size          = var.max-capacity
  min_size          = var.min-capacity
  force_delete      = true
  depends_on        = [aws_lb.application-lb]
  target_group_arns = [aws_lb_target_group.target-group.arn]
  health_check_type = "ELB"

  launch_template {
    id      = aws_launch_template.app_server.id
    version = "$Latest"
  }

  vpc_zone_identifier = var.private_subnet_ids
}

// ----------------------------------------------------------------------
// auto scaling policies
// ----------------------------------------------------------------------
resource "aws_autoscaling_policy" "cpu_up" {
  name                   = "${var.project_name}-cpu_over_65"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.asg.name
}
resource "aws_cloudwatch_metric_alarm" "cpu_alarm_up" {
  alarm_name          = "${var.project_name}-cpu_alarm_up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "65"
  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.asg.name
  }
  alarm_actions = [aws_autoscaling_policy.cpu_up.arn]
}

resource "aws_autoscaling_policy" "cpu_down" {
  name                   = "${var.project_name}-cpu_under_40"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.asg.name
}
resource "aws_cloudwatch_metric_alarm" "cpu_alarm_down" {
  alarm_name          = "${var.project_name}-cpu_alarm_down"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "40"
  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.asg.name
  }
  alarm_actions = [aws_autoscaling_policy.cpu_down.arn]
}

// ----------------------------------------------------------------------
// defines target group
// ----------------------------------------------------------------------
resource "aws_lb_target_group" "target-group" {
  health_check {
    enabled             = true
    interval            = 300
    path                = "/"
    timeout             = 60
    matcher             = 200
    healthy_threshold   = 5
    unhealthy_threshold = 5
  }
  name        = "${var.project_name}-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = var.vpc_id
}

// ----------------------------------------------------------------------
// application load balancer
// ----------------------------------------------------------------------
resource "aws_lb" "application-lb" {
  name               = "${var.project_name}-alb"
  internal           = false
  ip_address_type    = "ipv4"
  load_balancer_type = "application"
  security_groups    = [var.alb_security_group]
  subnets            = var.subnet_ids

  tags = {
    Environment = var.environment
  }
}

// ----------------------------------------------------------------------
// alb listener
// ----------------------------------------------------------------------

resource "aws_lb_listener" "alb-listener" {
  load_balancer_arn = aws_lb.application-lb.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target-group.arn
  }
}