// --------------------------------------------------
// Database (RDS) variables
// --------------------------------------------------
variable "project_name" {
  type        = string
  description = "This configures the project name"
}
variable "instance_class" {
  description = "Database Instance Class"
  type        = string
}
variable "db_storage_size" {
  description = "Database storage size"
  type        = number
}
variable "identifier" {
  type        = string
  description = "This configures the db instance name"
}
variable "engine" {
  type        = string
  description = "This configures the db engine"
}
variable "engine_version" {
  type        = string
  description = "This configures the db engine version"
}
variable "db_name" {
  type        = string
  description = "This configures the database name"
}
variable "username" {
  description = "Enter - This configures the master user of the database"
  type        = string
  sensitive   = true
}
variable "password" {
  description = "Enter - This configures the master password of the database"
  type        = string
  sensitive   = true
}
variable "database-security-group" {
  description = "Confiures database security group"
}
variable "private_subnet1_az1" {
  description = "Configures private subnet 1 for database subnet group"
}
variable "private_subnet1_az2" {
  description = "Configures private subnet 2 for database subnet group"
}

variable "environment" {
  type        = string
  description = "Environment tag"
}
