variable "project_name" {
  type        = string
  description = "This configures the project name"
}
variable "vpc_cidr" {
  type        = string
  description = "This configures the vpc cidr"
}

variable "environment" {
  type        = string
  description = "Environment tag"
}