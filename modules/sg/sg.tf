// --------------------------------------------------
// load balancer security group
// --------------------------------------------------

resource "aws_security_group" "alb_security_group" {
  name        = "alb security group"
  description = "enable http access on port 80"
  vpc_id      = var.vpc_id


  ingress {
    description = "http access"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [var.allowed_ip_alb]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "Application LB Security Group"
    Project     = var.project_name
    Environment = var.environment
  }
}

// --------------------------------------------------
// app servers security group
// --------------------------------------------------

resource "aws_security_group" "ec2_security_group" {
  name        = "ec2 security group"
  description = "enable http access on port 80 via alb sg"
  vpc_id      = var.vpc_id


  ingress {
    description     = "http access"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = ["${aws_security_group.alb_security_group.id}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "App server Security Group"
    Project     = var.project_name
    Environment = var.environment
  }
}

// --------------------------------------------------
// database security group
// --------------------------------------------------

resource "aws_security_group" "database-security-group" {
  name        = "Database Security Group"
  description = "Enable MYSQL access on port 3306 "
  vpc_id      = var.vpc_id


  ingress {
    description     = "MYSQL access"
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = ["${aws_security_group.ec2_security_group.id}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "Database Security Group"
    Project     = var.project_name
    Environment = var.environment
  }
}
