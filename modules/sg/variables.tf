variable "vpc_id" {
  type        = string
  description = "This configures the vpc id"
}

variable "environment" {
  type        = string
  description = "Environment tag"
}

variable "project_name" {
  type        = string
  description = "This configures the project name"
}

variable "allowed_ip_alb" {
  type        = string
  description = "This configures the project name"
}