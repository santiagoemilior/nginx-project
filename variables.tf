// vpc variables
variable "region" {
  type        = string
  description = "This defines the region"
}
variable "project_name" {
  type        = string
  description = "This configures the project name"
}
variable "vpc_cidr" {
  type        = string
  description = "This configures the vpc cidr"
}


// asg variables
variable "web_instance_type" {
  type        = string
  description = "This configures app server type"
}
variable "desired-capacity" {
  type        = number
  description = "This configures the desired capacity of our asg"
}
variable "max-capacity" {
  type        = number
  description = "This configures the max capacity of our asg"
}
variable "min-capacity" {
  type        = number
  description = "This configures the min capacity of our asg"
}
variable "ec2_web_tag_name" {
  type        = string
  description = "This adds a tag to the intances"
}

variable "allowed_ip_alb" {
  type        = string
  description = "This configures the project name"
}

// database variables
variable "identifier" {
  type        = string
  description = "This configures the db instance name"
}
variable "engine" {
  type        = string
  description = "This configures the db engine"
}
variable "engine_version" {
  type        = string
  description = "This configures the db engine version"
}
variable "db_name" {
  type        = string
  description = "This configures the database name"
}
variable "username" {
  description = "Enter - This configures the master user of the database"
  type        = string
  sensitive   = true
}
variable "password" {
  description = "Enter - This configures the master password of the database"
  type        = string
  sensitive   = true
}
variable "db_storage_size" {
  description = "This configures the database storage size"
  type        = number
}
variable "instance_class" {
  description = "This configures the database Instance Class"
  type        = string
}
variable "environment" {
  type        = string
  description = "Environment tag"
}