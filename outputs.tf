output "ALB" {
  description = "Load balancer dns name"
  value       = module.asg.load_balancer_dns
}

output "RDS_Endpoint" {
  description = "Endpoint of database "
  value       = module.database.endpoint
}